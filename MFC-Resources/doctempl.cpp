// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (C) Microsoft Corporation
// All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.

#include "stdafx.h"



#define new DEBUG_NEW

/////////////////////////////////////////////////////////////////////////////
// CDocTemplate 构造/析构

CDocTemplate::CDocTemplate(UINT nIDResource, CRuntimeClass* pDocClass,
	CRuntimeClass* pFrameClass, CRuntimeClass* pViewClass)
{
	ASSERT_VALID_IDR(nIDResource);
	ASSERT(pDocClass == NULL ||
		pDocClass->IsDerivedFrom(RUNTIME_CLASS(CDocument)));
	ASSERT(pFrameClass == NULL ||
		pFrameClass->IsDerivedFrom(RUNTIME_CLASS(CFrameWnd)));
	ASSERT(pViewClass == NULL ||
		pViewClass->IsDerivedFrom(RUNTIME_CLASS(CView)));

	m_nIDResource = nIDResource;
	m_nIDServerResource = NULL;
	m_nIDEmbeddingResource = NULL;
	m_nIDContainerResource = NULL;
	m_nIDPreviewResource = NULL;

	m_pDocClass = pDocClass;
	m_pFrameClass = pFrameClass;
	m_pViewClass = pViewClass;
	m_pOleFrameClass = NULL;
	m_pOleViewClass = NULL;

	
	m_pPreviewFrameClass = NULL;
	m_pPreviewViewClass = NULL;

	m_pAttachedFactory = NULL;
	m_hMenuInPlace = NULL;
	m_hAccelInPlace = NULL;
	m_hMenuEmbedding = NULL;
	m_hAccelEmbedding = NULL;
	m_hMenuInPlaceServer = NULL;
	m_hAccelInPlaceServer = NULL;

	// add to pStaticList if constructed as static instead of on heap；如果构造为静态而不是堆在堆上，那么添加到pstatiif
	if (CDocManager::bStaticInit)
	{
		m_bAutoDelete = FALSE;
		if (CDocManager::pStaticList == NULL)
			CDocManager::pStaticList = new CPtrList;
		if (CDocManager::pStaticDocManager == NULL)
			CDocManager::pStaticDocManager = new CDocManager;
		CDocManager::pStaticList->AddTail(this);
	}
	else
	{
		m_bAutoDelete = TRUE;   // 通常在堆上分配
		LoadTemplate();
	}
}

void CDocTemplate::LoadTemplate()
{
	//为m_strDocString赋值
	if (m_strDocStrings.IsEmpty() && !m_strDocStrings.LoadString(m_nIDResource))
	{
		TRACE(traceAppMsg, 0, "Warning: no document names in string for template #%d.\n",
			m_nIDResource);
	}

	if (m_nIDEmbeddingResource != 0 && m_hMenuEmbedding == NULL)
	{
		// load menu to be used while editing an embedding (as a server)
		HINSTANCE hInst = AfxFindResourceHandle(
			MAKEINTRESOURCE(m_nIDEmbeddingResource), RT_MENU);
		m_hMenuEmbedding =
			::LoadMenuW(hInst, MAKEINTRESOURCEW(m_nIDEmbeddingResource));
		m_hAccelEmbedding =
			::LoadAcceleratorsW(hInst, MAKEINTRESOURCEW(m_nIDEmbeddingResource));
	}
	if (m_nIDServerResource != 0 && m_hMenuInPlaceServer == NULL)
	{
		// load menu to be used while editing in-place (as a server)
		HINSTANCE hInst = AfxFindResourceHandle(
			MAKEINTRESOURCE(m_nIDServerResource), RT_MENU);
		m_hMenuInPlaceServer = ::LoadMenuW(hInst,
			MAKEINTRESOURCEW(m_nIDServerResource));
		m_hAccelInPlaceServer = ::LoadAcceleratorsW(hInst,
			MAKEINTRESOURCEW(m_nIDServerResource));
	}

	if (m_nIDContainerResource != 0 && m_hMenuInPlace == NULL)
	{
		// load menu to be used while in-place editing session (as a container)
		HINSTANCE hInst = AfxFindResourceHandle(
			MAKEINTRESOURCE(m_nIDContainerResource), RT_MENU);
		m_hMenuInPlace = ::LoadMenuW(hInst,
			MAKEINTRESOURCEW(m_nIDContainerResource));
		m_hAccelInPlace = ::LoadAcceleratorsW(hInst,
			MAKEINTRESOURCEW(m_nIDContainerResource));
	}
}

void CDocTemplate::SetServerInfo(UINT nIDOleEmbedding, UINT nIDOleInPlaceServer,
	CRuntimeClass* pOleFrameClass, CRuntimeClass* pOleViewClass)
{
	ASSERT_VALID_IDR(nIDOleEmbedding);
	if (nIDOleInPlaceServer != 0)
		ASSERT_VALID_IDR(nIDOleInPlaceServer);
	ASSERT(pOleFrameClass == NULL ||
		pOleFrameClass->IsDerivedFrom(RUNTIME_CLASS(CFrameWnd)));
	ASSERT(pOleViewClass == NULL ||
		pOleViewClass->IsDerivedFrom(RUNTIME_CLASS(CView)));

	m_pOleFrameClass = pOleFrameClass;
	m_pOleViewClass = pOleViewClass;

	m_nIDEmbeddingResource = nIDOleEmbedding;
	m_nIDServerResource = nIDOleInPlaceServer;
	if (!CDocManager::bStaticInit)
		LoadTemplate();
}

void CDocTemplate::SetContainerInfo(UINT nIDOleInPlaceContainer)
{
	ASSERT(nIDOleInPlaceContainer != 0);

	m_nIDContainerResource = nIDOleInPlaceContainer;
	if (!CDocManager::bStaticInit)
		LoadTemplate();
}

void CDocTemplate::SetPreviewInfo (UINT nIDPreviewFrame, CRuntimeClass* pPreviewFrameClass, CRuntimeClass* pPreviewViewClass)
{
	if (nIDPreviewFrame != 0)
		ASSERT_VALID_IDR(nIDPreviewFrame);
	ASSERT(pPreviewFrameClass == NULL || pPreviewFrameClass->IsDerivedFrom (RUNTIME_CLASS(CFrameWnd)));
	ASSERT(pPreviewViewClass == NULL || pPreviewViewClass->IsDerivedFrom (RUNTIME_CLASS(CView)));

	m_nIDPreviewResource = nIDPreviewFrame;
	m_pPreviewFrameClass = pPreviewFrameClass;
	m_pPreviewViewClass = pPreviewViewClass;
}

CDocTemplate::~CDocTemplate()
{
	// delete OLE resources
	if (m_hMenuInPlace != NULL)
		::DestroyMenu(m_hMenuInPlace);
	if (m_hAccelInPlace != NULL)
		::FreeResource(m_hAccelInPlace);
	if (m_hMenuEmbedding != NULL)
		::DestroyMenu(m_hMenuEmbedding);
	if (m_hAccelEmbedding != NULL)
		::FreeResource(m_hAccelEmbedding);
	if (m_hMenuInPlaceServer != NULL)
		::DestroyMenu(m_hMenuInPlaceServer);
	if (m_hAccelInPlaceServer != NULL)
		::FreeResource(m_hAccelInPlaceServer);
}

/////////////////////////////////////////////////////////////////////////////
// CDocTemplate attributes

BOOL CDocTemplate::GetDocString(CString& rString, enum DocStringIndex i) const
{
	return AfxExtractSubString(rString, m_strDocStrings, (int)i);
}

/////////////////////////////////////////////////////////////////////////////
// Document management

void CDocTemplate::AddDocument(CDocument* pDoc)
{
	ASSERT_VALID(pDoc);
	ASSERT(pDoc->m_pDocTemplate == NULL);   // no template attached yet
	pDoc->m_pDocTemplate = this;
}

void CDocTemplate::RemoveDocument(CDocument* pDoc)
{
	ASSERT_VALID(pDoc);
	ASSERT(pDoc->m_pDocTemplate == this);   // must be attached to us
	pDoc->m_pDocTemplate = NULL;
}

CDocTemplate::Confidence CDocTemplate::MatchDocType(LPCTSTR lpszPathName,
	CDocument*& rpDocMatch)
{
	ASSERT(lpszPathName != NULL);
	rpDocMatch = NULL;

	// 通过所有文档
	POSITION pos = GetFirstDocPosition();
	while (pos != NULL)
	{
		CDocument* pDoc = GetNextDoc(pos);
		if (AfxComparePath(pDoc->GetPathName(), lpszPathName))//比较两个路径是否一样
		{
			// 已经打开
			rpDocMatch = pDoc;
			return yesAlreadyOpen;
		}
	}

	// 看看它是否匹配我们的默认后缀
	CString strFilterExt;

	if (GetDocString(strFilterExt, CDocTemplate::filterExt) &&//提取子字符串中第filterEx段的字符并保存在strFilterExt
	  !strFilterExt.IsEmpty())
	{
		// 看看扩展匹配
		ASSERT(strFilterExt[0] == '.');
		LPCTSTR lpszDot = ::PathFindExtension(lpszPathName);//获取路径文件的后缀名
		if (lpszDot != NULL)
        {
            if(::AfxComparePath(lpszDot, static_cast<const TCHAR *>(strFilterExt)))
            {
			    return yesAttemptNative; // 扩展匹配，看起来像我们的
            }
        }
	}

	// 否则我们会猜测它不相关
	return yesAttemptForeign;
}

CDocument* CDocTemplate::CreateNewDocument()
{
	// default implementation constructs one from CRuntimeClass
	if (m_pDocClass == NULL)
	{
		TRACE(traceAppMsg, 0, "Error: you must override CDocTemplate::CreateNewDocument.\n");
		ASSERT(FALSE);
		return NULL;
	}
	CDocument* pDocument = (CDocument*)m_pDocClass->CreateObject();
	if (pDocument == NULL)
	{
		TRACE(traceAppMsg, 0, "Warning: Dynamic create of document type %hs failed.\n",
			m_pDocClass->m_lpszClassName);
		return NULL;
	}
	ASSERT_KINDOF(CDocument, pDocument);
	AddDocument(pDocument);
	return pDocument;
}

/////////////////////////////////////////////////////////////////////////////
// 创建默认窗口

CFrameWnd* CDocTemplate::CreateNewFrame(CDocument* pDoc, CFrameWnd* pOther)
{
	if (pDoc != NULL)
		ASSERT_VALID(pDoc);
	// 创建连接到指定文档的框架

	ASSERT(m_nIDResource != 0); // 必须有一个资源ID来初始化窗口

	CCreateContext context;
	context.m_pCurrentFrame = pOther;
	context.m_pCurrentDoc = pDoc;
	context.m_pNewViewClass = m_pViewClass;
	context.m_pNewDocTemplate = this;

	if (m_pFrameClass == NULL)
	{
		TRACE(traceAppMsg, 0, "Error: you must override CDocTemplate::CreateNewFrame.\n");
		ASSERT(FALSE);
		return NULL;
	}
	CFrameWnd* pFrame = (CFrameWnd*)m_pFrameClass->CreateObject();
	if (pFrame == NULL)
	{
		TRACE(traceAppMsg, 0, "Warning: Dynamic create of frame %hs failed.\n",
			m_pFrameClass->m_lpszClassName);
		return NULL;
	}
	ASSERT_KINDOF(CFrameWnd, pFrame);

	if (context.m_pNewViewClass == NULL)
		TRACE(traceAppMsg, 0, "Warning: creating frame with no default view.\n");

	// 创建新的资源，原始的视图/框架
	if (!pFrame->LoadFrame(m_nIDResource,
		WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE,   // 默衣窗口风格
		NULL, &context))
	{
		TRACE(traceAppMsg, 0, "Warning: CDocTemplate couldn't create a frame.\n");
		// 窗口将被删除，在 PostNcDestroy 中清理
		return NULL;
	}

	// 窗口开始工作了
	return pFrame;
}
CFrameWnd* CDocTemplate::CreateOleFrame(CWnd* pParentWnd, CDocument* pDoc,
	BOOL bCreateView)
{
	CCreateContext context;
	context.m_pCurrentFrame = NULL;
	context.m_pCurrentDoc = pDoc;
	context.m_pNewViewClass = bCreateView ? m_pOleViewClass : NULL;
	context.m_pNewDocTemplate = this;

	if (m_pOleFrameClass == NULL)
	{
		TRACE(traceAppMsg, 0, "Warning: pOleFrameClass not specified for doc template.\n");
		return NULL;
	}

	ASSERT(m_nIDServerResource != 0); // must have a resource ID to load from
	CFrameWnd* pFrame = (CFrameWnd*)m_pOleFrameClass->CreateObject();
	if (pFrame == NULL)
	{
		TRACE(traceAppMsg, 0, "Warning: Dynamic create of frame %hs failed.\n",
			m_pOleFrameClass->m_lpszClassName);
		return NULL;
	}

	// create new from resource (OLE frames are created as child windows)
	if (!pFrame->LoadFrame(m_nIDServerResource,
		WS_CHILD|WS_CLIPSIBLINGS, pParentWnd, &context))
	{
		TRACE(traceAppMsg, 0, "Warning: CDocTemplate couldn't create an OLE frame.\n");
		// frame will be deleted in PostNcDestroy cleanup
		return NULL;
	}

	// it worked !
	return pFrame;
}

CFrameWnd* CDocTemplate::CreatePreviewFrame(CWnd* pParentWnd, CDocument* pDoc)
{
	CCreateContext context;
	context.m_pCurrentFrame = NULL;
	context.m_pCurrentDoc = pDoc;
	context.m_pNewViewClass = m_pPreviewViewClass != NULL ? m_pPreviewViewClass : m_pViewClass;

	CRuntimeClass* pFrameClass = m_pPreviewFrameClass != NULL ? m_pPreviewFrameClass : RUNTIME_CLASS (CFrameWnd);
	VERIFY(pFrameClass != NULL);

	CFrameWnd* pFrame = (CFrameWnd*)pFrameClass->CreateObject();
	if (pFrame == NULL)
	{
		TRACE0("Warning: Dynamic create of frame %hs failed.\n");
		return NULL;
	}
	ASSERT_KINDOF(CFrameWnd, pFrame);

	if (m_nIDPreviewResource != 0)
	{
		// create new from resource (preview frames are created as child windows)
		if (!pFrame->LoadFrame(m_nIDPreviewResource,
			WS_CHILD|WS_CLIPSIBLINGS, pParentWnd, &context))
		{
			TRACE(traceAppMsg, 0, "Warning: CDocTemplate couldn't create a preview frame.\n");
			// frame will be deleted in PostNcDestroy cleanup
			return NULL;
		}
	}
	else
	{
		// create dummy frame
		CREATESTRUCT cs;
		memset(&cs, 0, sizeof(CREATESTRUCT));
		cs.style = WS_CHILD|WS_CLIPSIBLINGS;

		VERIFY(AfxDeferRegisterClass(AFX_WNDFRAMEORVIEW_REG));
		cs.lpszClass = _afxWndFrameOrView;  // COLOR_WINDOW background

		WNDCLASS wndcls;
		if (cs.lpszClass != NULL && GetClassInfo(AfxGetInstanceHandle(), cs.lpszClass, &wndcls))
		{
			// register a very similar WNDCLASS
			LPCTSTR lpcszClassName = AfxRegisterWndClass(wndcls.style, wndcls.hCursor, wndcls.hbrBackground);

			CRect rectEmpty; 
			rectEmpty.SetRectEmpty ();

			if (!pFrame->Create(lpcszClassName, _T (""), cs.style, rectEmpty,
				pParentWnd, 0, 0L, &context))
			{
				TRACE(traceAppMsg, 0, "Warning: CDocTemplate couldn't create a preview frame.\n");
				// frame will be deleted in PostNcDestroy cleanup
				return FALSE;   // will self destruct on failure normally
			}
		}
	}

	return pFrame;
}

void CDocTemplate::InitialUpdateFrame(CFrameWnd* pFrame, CDocument* pDoc,
	BOOL bMakeVisible)
{
	// just delagate to implementation in CFrameWnd
	pFrame->InitialUpdateFrame(pDoc, bMakeVisible);
}

/////////////////////////////////////////////////////////////////////////////
// CDocTemplate commands and command helpers

BOOL CDocTemplate::SaveAllModified()
{
	POSITION pos = GetFirstDocPosition();
	while (pos != NULL)
	{
		CDocument* pDoc = GetNextDoc(pos);
		if (!pDoc->SaveModified())
			return FALSE;
	}
	return TRUE;
}


void CDocTemplate::CloseAllDocuments(BOOL)
{
	POSITION pos = GetFirstDocPosition();
	while (pos != NULL)
	{
		CDocument* pDoc = GetNextDoc(pos);
		pDoc->OnCloseDocument();
	}
}

void CDocTemplate::OnIdle()
{
	POSITION pos = GetFirstDocPosition();
	while (pos != NULL)
	{
		CDocument* pDoc = GetNextDoc(pos);
		ASSERT_VALID(pDoc);
		ASSERT_KINDOF(CDocument, pDoc);
		pDoc->OnIdle();
	}
}

BOOL CDocTemplate::OnCmdMsg(UINT nID, int nCode, void* pExtra,
	AFX_CMDHANDLERINFO* pHandlerInfo)
{
	BOOL bReturn;
	CCmdTarget* pFactory = DYNAMIC_DOWNCAST(CCmdTarget, m_pAttachedFactory);

	if (nCode == CN_OLE_UNREGISTER && pFactory != NULL)
		bReturn = pFactory->OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
	else
		bReturn = CCmdTarget::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);

	return bReturn;
}

/////////////////////////////////////////////////////////////////////////////
// CDocTemplate diagnostics

#ifdef _DEBUG
void CDocTemplate::Dump(CDumpContext& dc) const
{
	CCmdTarget::Dump(dc);

	dc << "m_nIDResource = " << m_nIDResource;
	dc << "\nm_strDocStrings: " << m_strDocStrings;

	if (m_pDocClass)
		dc << "\nm_pDocClass = " << m_pDocClass->m_lpszClassName;
	else
		dc << "\nm_pDocClass = NULL";

	if (dc.GetDepth() > 0)
	{
		dc << "\ndocument list = {";
		POSITION pos = GetFirstDocPosition();
		while (pos != NULL)
		{
			CDocument* pDoc = GetNextDoc(pos);
			dc << "\ndocument " << pDoc;
		}
		dc << "\n}";
	}

	dc << "\n";
}

void CDocTemplate::AssertValid() const
{
	CCmdTarget::AssertValid();

	POSITION pos = GetFirstDocPosition();
	while (pos != NULL)
	{
		CDocument* pDoc = GetNextDoc(pos);
		ASSERT_VALID(pDoc);
	}
}
#endif //_DEBUG


IMPLEMENT_DYNAMIC(CDocTemplate, CCmdTarget)

/////////////////////////////////////////////////////////////////////////////
